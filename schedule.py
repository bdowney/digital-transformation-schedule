''' Schedule Generator for Digital Transformation'''

# Modules
from datetime import timedelta, datetime
import argparse
import processscheduler as ps

def main():
    ''' Main Schedule Builder. Solving and Outputs moved to own functions.'''

    # Global Values
    project_start_date = datetime(2021, 7, 1) # Year, Month, Day
    delta = timedelta(weeks=1) # Task value (i.e. a task that has a value of 1 is one week.)
    digital_transformation = ps.SchedulingProblem('DigitalTransformation', delta_time=delta, start_time=project_start_date)

    # Teams and Resources
    # NUM_DEV_TEAMS = 10 # Set via parser default below.
    num_cust_admin = 2
    num_gitlab_coe_master = 2
    num_gitlab_tam = 1
    num_gitlab_resident = 1
    num_gitlab_learn = 1
    migrate_before_learn = False

    # Resources
    r_dev_team = [ps.Worker('DevTeam_{:02d}'.format(i + 1)) for i in range(NUM_DEV_TEAMS)]
    r_cust_admin = [ps.Worker('CustAdmin_%i' % (i + 1)) for i in range(num_cust_admin)]
    r_gitlab_coe_master = [ps.Worker('GitLab_CoE_Master_%i' % (i + 1)) for i in range(num_gitlab_coe_master)]
    r_gitlab_tam = [ps.CumulativeWorker('GitLab_TAM_%i' % (i + 1), size=4) for i in range(num_gitlab_tam)] # TAM is an oversight resource and can multi-task.
    r_gitlab_resident = [ps.Worker('GitLab_Resident_%i' % (i + 1)) for i in range(num_gitlab_resident)]
    r_gitlab_learn = [ps.Worker('GitLab_Learn_%i' % (i + 1)) for i in range(num_gitlab_learn)]

    # Project Tasks (All values are incrament of delta)

    # HA Design and Build
    t_design_build_ha = ps.FixedDurationTask('Design_Build_HA', duration=2, priority=100)
    t_design_build_ha.add_required_resource(ps.SelectWorkers(r_cust_admin, nb_workers_to_select=1, kind='exact'))
    t_design_build_ha.add_required_resource(ps.SelectWorkers(r_gitlab_tam, nb_workers_to_select=1, kind='exact'))
    t_design_build_ha.add_required_resource(ps.SelectWorkers(r_gitlab_resident, nb_workers_to_select=1, kind='exact'))

    # Build L&D Program
    t_build_learn_platform = ps.FixedDurationTask('Build_Learn_Platform', duration=2, priority=100)
    t_build_learn_platform.add_required_resource(ps.SelectWorkers(r_gitlab_tam, nb_workers_to_select=1, kind='exact'))
    t_build_learn_platform.add_required_resource(ps.SelectWorkers(r_gitlab_learn, nb_workers_to_select=1, kind='exact'))

    # Team Instance Migration
    ts_team_migration = [ps.FixedDurationTask('%s_Migrate' % (i.name), duration=1, priority=20) for i in r_dev_team]
    for t_team_migration in ts_team_migration:
        t_team_migration.add_required_resource(ps.SelectWorkers(r_cust_admin))
        t_team_migration.add_required_resource(ps.SelectWorkers(r_gitlab_resident))
        digital_transformation.add_constraint(ps.TaskPrecedence(t_design_build_ha, t_team_migration, kind='strict'))

    # Team Learning and Development (async)
    ts_team_learn = [ps.FixedDurationTask('%s_Learn' % (i.name), duration=1, priority=10) for i in r_dev_team]
    team_num = 0
    for t_team_learn in ts_team_learn:
        t_team_learn.add_required_resource(r_dev_team[team_num])
        t_team_learn.add_required_resource(ps.SelectWorkers(r_gitlab_tam))
        digital_transformation.add_constraint(ps.TaskPrecedence(t_build_learn_platform, t_team_learn, kind='strict'))
        if migrate_before_learn:
            digital_transformation.add_constraint(ps.TaskPrecedence(ts_team_migration[team_num], t_team_learn, kind='strict'))
        team_num += 1

    # Dev Team Center of Excellence
    ts_team_coe = [ps.FixedDurationTask('%s_CoE' % (i.name), duration=2, priority=10) for i in r_dev_team]
    team_num = 0
    for t_team_coe in ts_team_coe:
        t_team_coe.add_required_resource(r_dev_team[team_num])
        t_team_coe.add_required_resource(ps.SelectWorkers(r_gitlab_coe_master, nb_workers_to_select=1, kind='exact'))
        digital_transformation.add_constraint(ps.TaskPrecedence(ts_team_learn[team_num], ts_team_coe[team_num], kind='strict'))
        digital_transformation.add_constraint(ps.TasksDontOverlap(ts_team_migration[team_num], ts_team_coe[team_num]))
        team_num += 1

    return digital_transformation

def solve(problem):
    ''' Solve Function for SchedulingSolver '''
    solver = ps.SchedulingSolver(problem, logics="QF_UFIDL", max_time=MAX_TIME_TO_SOLVE, parallel=True)
    return solver.solve()

if __name__ == '__main__':
    # Check for output file when run from command line.
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--output", help="Output File Name", default=None)
    parser.add_argument("-t", "--teams", help="Number of Dev Teams", type=int, default=10)
    parser.add_argument("-m", "--max_time", help="Max Time to Spend looking for the best solution", type=int, default=30)
    args = parser.parse_known_args()

    outputfile = args[0].output
    NUM_DEV_TEAMS = args[0].teams
    MAX_TIME_TO_SOLVE = args[0].max_time

    dt = main()
    dt.add_objective_priorities()
    solution = solve(dt)
    if outputfile:
        h = max(600, int(len(solution.get_scheduled_tasks()) * 50))
        solution.render_gantt_plotly(render_mode='Resource', sort="Task", fig_size=(h,h), html_filename=outputfile)
