#ProcessScheduler>=0.6.1
git+git://github.com/tpaviot/ProcessScheduler.git@master#egg=ProcessScheduler
z3-solver>=4.8.10.0
plotly>=4.14.3
numpy>=1.19.5
ipykernel>=5.5.5
nbformat>=4.2.0