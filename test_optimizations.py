''' Testing of different optimization settings '''

import unittest

import schedule

output_results = [("TEST NAME\t\t", "TEAMS", "TIME", "INTERVALS")]

class TestDigitalTransformation(unittest.TestCase):
    '''Tests of different number of teams and timeout values with optimizations.'''
    @classmethod
    def tearDownClass(cls):
        print("\n")
        for row in output_results:
            print(f"{row[0]}\t{row[1]}\t{row[2]}\t{row[3]}")

    def test_dt_optimization_none(self) -> None:
        ''' Basic test with no optimizations '''
        teams_time = [
            (10,30),
            (20,30),
            (40,60),
        ]
        for test_values in teams_time:
            with self.subTest(test_values=test_values):
                schedule.NUM_DEV_TEAMS = test_values[0]
                schedule.MAX_TIME_TO_SOLVE = test_values[1]
                dt_problem = schedule.main()
                solution = schedule.solve(dt_problem)
                self.assertTrue(solution)
                print(f"Teams: {schedule.NUM_DEV_TEAMS}")
                print(f"Max Time: {schedule.MAX_TIME_TO_SOLVE}")
                print(f"Total intervals: {solution.horizon}")
                output_results.append(("test_dt_optimization_none", schedule.NUM_DEV_TEAMS, schedule.MAX_TIME_TO_SOLVE, solution.horizon))

    def test_dt_optimization_priority(self) -> None:
        ''' Testing with priority optimization '''
        teams_time = [
            (10,30),
            (20,30),
            (40,60),
        ]
        for test_values in teams_time:
            with self.subTest(test_values=test_values):
                schedule.NUM_DEV_TEAMS = test_values[0]
                schedule.MAX_TIME_TO_SOLVE = test_values[1]
                dt_problem = schedule.main()
                dt_problem.add_objective_priorities()
                solution = schedule.solve(dt_problem)
                self.assertTrue(solution)
                print(f"Teams: {schedule.NUM_DEV_TEAMS}")
                print(f"Max Time: {schedule.MAX_TIME_TO_SOLVE}")
                print(f"Total intervals: {solution.horizon}")
                output_results.append(("test_dt_optimization_priority", schedule.NUM_DEV_TEAMS, schedule.MAX_TIME_TO_SOLVE, solution.horizon))

    def test_dt_optimization_span(self) -> None:
        ''' Testing with span optimization '''
        teams_time = [
            (10,30),
            (20,30),
            (40,60),
        ]
        for test_values in teams_time:
            with self.subTest(test_values=test_values):
                schedule.NUM_DEV_TEAMS = test_values[0]
                schedule.MAX_TIME_TO_SOLVE = test_values[1]
                dt_problem = schedule.main()
                dt_problem.add_objective_makespan()
                solution = schedule.solve(dt_problem)
                self.assertTrue(solution)
                print(f"Teams: {schedule.NUM_DEV_TEAMS}")
                print(f"Max Time: {schedule.MAX_TIME_TO_SOLVE}")
                print(f"Total intervals: {solution.horizon}")
                output_results.append(("test_dt_optimization_span", schedule.NUM_DEV_TEAMS, schedule.MAX_TIME_TO_SOLVE, solution.horizon))

    def test_dt_optimization_span_priority(self) -> None:
        ''' Testing with span optimization '''
        teams_time = [
            (10,30),
            (20,30),
            (40,60),
        ]
        for test_values in teams_time:
            with self.subTest(test_values=test_values):
                schedule.NUM_DEV_TEAMS = test_values[0]
                schedule.MAX_TIME_TO_SOLVE = test_values[1]
                dt_problem = schedule.main()
                dt_problem.add_objective_makespan()
                dt_problem.add_objective_priorities()
                solution = schedule.solve(dt_problem)
                self.assertTrue(solution)
                print(f"Teams: {schedule.NUM_DEV_TEAMS}")
                print(f"Max Time: {schedule.MAX_TIME_TO_SOLVE}")
                print(f"Total intervals: {solution.horizon}")
                output_results.append(("test_dt_optimization_span_priority", schedule.NUM_DEV_TEAMS, schedule.MAX_TIME_TO_SOLVE, solution.horizon))


if __name__ == '__main__':
    unittest.main()
